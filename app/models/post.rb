class Post < ActiveRecord::Base
  has_many :comments, dependent: :destroy #want to delete all comments when del post
  validates_presence_of :title, :body
end
