class Comment < ActiveRecord::Base
  belongs_to :post
  validates_presence_of :post_id, :body
  #next line ok for early version, before comments placed in posts/show page
  #validates :post_id, :numericality => { :greater_than => 0}
end
